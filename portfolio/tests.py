from django.test import TestCase
from . models import Project

# Create your tests here.
class ModelTesting(TestCase):
    
    def setUp(self):
      self.project = Project.objects.create(title='blog1', descr='Test of blog1')
    
    def test_project_model(self):
      d = self.project
      self.assertTrue(isinstance(d, Project))
      self.assertEqual(str(d), 'blog1')
